package clantraWeb;

import java.io.IOException;
import java.lang.reflect.Method;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import atu.testrecorder.exceptions.ATUTestRecorderException;

public class WebTest extends TestBase {
	static final Logger logger = Logger.getLogger(WebTest.class.getName());

	ClantraHomePageLogin objHomePage;
	ClantraLogOut objLogout;
	ClantraAttendance objMarkingAttendance;
	ClantraLoginError objErrorMessage;

	@BeforeSuite
	@Parameters("browser")
	public void setup(String browser) throws IOException {
		super.setup(browser);

		objHomePage = new ClantraHomePageLogin();
		objLogout = new ClantraLogOut();
		objMarkingAttendance = new ClantraAttendance();
		objErrorMessage = new ClantraLoginError();
	}

	@BeforeMethod
	public void beforeMethodSetup(Method method) {
		test = extent.startTest(method.getName().toString());
		test.assignCategory(browser);
		try {
			// String browserName = CommonUtils.getBrowserDetails();
			startRecording(method.getName().toString());

		} catch (ATUTestRecorderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// *************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************
	// ADCA Test cases
	// Test cases -- Verify login and mark attendance
	@Test(priority = 0)
	public void verifyLogin() {
		try {
			loadUrl(props.getProperty("clantraLandingPageUrl"), props.getProperty("clantraSitetitle"));
			objHomePage.loginClantra(TestData.EMAIL);
			test.log(LogStatus.PASS, "User was able to navigate to Login page successfully");
		} catch (Exception e) {
			Assert.fail(); // To fail test in case of any element identification
			// failure
			test.log(LogStatus.FAIL, "User was not able to navigate to Login page successfully");
			assert (false);
		}
		try {
			objMarkingAttendance.markingAttendance();
			test.log(LogStatus.PASS, "User marked Attendance Successfully");
		} catch (Exception e) {
			Assert.fail(); // To fail test in case of any element identification
			// failure
			test.log(LogStatus.FAIL, "User was not able to mark Attendance Successfully");
			assert (false);
		}
		try {
			objLogout.logOut();
			test.log(LogStatus.PASS, "User Log Out Successfully");
		} catch (Exception e) {
			Assert.fail(); // To fail test in case of any element identification
			// failure
			test.log(LogStatus.FAIL, "User was not able to Logout Successfully");
			assert (false);
		}
	}
	
		// Test cases -- Verify error message while login
		@Test(priority = 1)
		public void verifyErrorLogin() {
			try {
				loadUrl(props.getProperty("clantraLandingPageUrl"), props.getProperty("clantraSitetitle"));
				objErrorMessage.loginErrorMessage(TestData.EMAIL_INVALID);
				test.log(LogStatus.PASS, "User was able to navigate to Login page error message");
			} catch (Exception e) {
				Assert.fail(); // To fail test in case of any element identification
				// failure
				test.log(LogStatus.FAIL, "User was not able to navigate to error message successfully");
				assert (false);
			}
		}
}
