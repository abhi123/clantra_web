package utils;

import java.util.HashMap;

import org.testng.Reporter;

public class CustomLogging {
    public static void writeToReport(String prefix, String testData){
        Reporter.log(prefix + " <span class='testdata'>"+testData+"</span>");
    }

    public static void writeToReport(String prefix){
        Reporter.log(prefix );
    }
    
    public static void writeToReport(String prefix, int testData){
        Reporter.log(prefix + " <span class='testdata'>"+testData+"</span>");
    }
    public static void writeToReport(String prefix, HashMap<String, String> exp){
        Reporter.log(prefix + " <span class='testdata'>"+exp+"</span>");
    }

}
