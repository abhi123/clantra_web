package utils;

import utils.CustomLogging;

import java.util.HashMap;

import org.testng.Assert;

public class CustomAssertions {

	public static void assertText(String exp, String act, String logPreFix) {
		Assert.assertEquals(exp, act);
		CustomLogging.writeToReport(logPreFix, exp);
	}

	public static void assertText(int exp, int act, String logPreFix) {
		Assert.assertEquals(exp, act);
		CustomLogging.writeToReport(logPreFix, exp);
	}

	public static void assertText(HashMap<String, String> exp, HashMap<String, String> act, String logPreFix) {
		Assert.assertEquals(exp, act);
		CustomLogging.writeToReport(logPreFix, exp);
	}

	public static void assertTrue(boolean act) {
		assertTrue(act, "");
	}

	public static void assertTrue(boolean act, String message) {
		if (act)
			CustomLogging.writeToReport("PASSED: " + message);
		else
			CustomLogging.writeToReport("FAILED: " + message);

	}

	public static void assertResponseCodeAs200(int responseCode, String message) {
		if (responseCode == 200)
			CustomLogging.writeToReport("PASSED: " + message + " : 200");
		else {
			CustomLogging.writeToReport("FAILED: " + message + " : " + responseCode);
			throw new AssertionError(
					"ASSERTION FAILED: Expected response code: 200, Actual response code: " + responseCode);
		}

	}

}
