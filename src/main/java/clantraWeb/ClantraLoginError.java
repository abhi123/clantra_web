package clantraWeb;

import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import api.test.GetApiResponse;
import api.test.PostApiResponse;
import utils.CustomAssertions;

public class ClantraLoginError extends CommonUtils {

	static final Logger LOGGER = Logger.getLogger(ClantraHomePageLogin.class.getName());
	PostApiResponse objPostApi = new PostApiResponse();
	GetApiResponse objGetApi = new GetApiResponse();

	// Objects
	By userEmailID = By.id("emailAddress");
	By userPass = By.id("password");
	By loginButton = By.id("login1");
	By loginErrorText = By.xpath("//span[@class='md-toast-text ng-binding']");
	                             
	Entry<String, String> apiResValues, apiBirthValue;

	// Method to login Clantra and Verify Email
	public void loginErrorMessage(String UUID) throws Exception {

		waitForPageLoad();
		if (checkElementPresence(userEmailID)) {
			waitFindEnterText(userEmailID, UUID);
			assert true;
		} else {
			assert false;
		}
		waitForPageLoad();

		if (checkElementPresence(userPass)) {
			waitFindEnterText(userPass, TestData.PASS);
			assert true;
		} else {
			assert false;
		}
		// sending password
		if (checkElementdisplayed(loginButton)) {
			waitFindClick(loginButton);
			assert true;
		} else {
			LOGGER.info("Not able click on login button");
			assert false;
		}
		waitForPageLoad();
		waitForElement(loginErrorText);
		CustomAssertions.assertText(textFromApplication(loginErrorText), TestData.LOGIN_ERROR,
				"Verified Error message as ");
	}

}
