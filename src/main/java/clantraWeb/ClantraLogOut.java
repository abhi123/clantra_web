package clantraWeb;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;

public class ClantraLogOut extends CommonUtils {

	static final Logger LOGGER = Logger.getLogger(ClantraLogOut.class.getName());

	// Objects
	By headerMenu = By.xpath("//i[@class='fa fa-caret-down caretDown']");
	By logOutButton = By.xpath("//div[@class='userLogout']");

	// Method to logout from Account
	public void logOut() throws InterruptedException {

		// sign out
		try {
			waitForElement(headerMenu);
			if (checkElementPresence(headerMenu)) {
				moveToWebElementAndActionClick(headerMenu);
				assert true;
			} else {
				assert false;
			}
			waitForElement(logOutButton);
			javaScriptScrollUsingBy(logOutButton);
			javaScriptClickUsingBy(logOutButton);
			waitForPageLoad();
			//Assert.assertEquals(TestData.LOGIN_URL, driver.getCurrentUrl());
		} catch (Exception e) {
			logger.info("Not able to Sign Out");
			logger.error(e);
			
		}
	}
}
