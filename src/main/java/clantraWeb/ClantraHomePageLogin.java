package clantraWeb;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import api.response.user.data.GetApiData;
import api.response.user.data.PostApiData;
import api.test.GetApiResponse;
import api.test.PostApiResponse;
import utils.CustomAssertions;
import utils.CustomLogging;

public class ClantraHomePageLogin extends CommonUtils {

	static final Logger LOGGER = Logger.getLogger(ClantraHomePageLogin.class.getName());
	PostApiResponse objPostApi = new PostApiResponse();
	GetApiResponse objGetApi = new GetApiResponse();

	// Objects
	By userEmailID = By.id("emailAddress");
	By userPass = By.id("password");
	By loginButton = By.id("login1");
	By loginSuccessText = By.xpath("//span[@class='md-toast-text ng-binding']");
	By emailText = By.xpath("//div[@class='userEmail truncate textCenter ng-binding']");
	By empId = By.xpath("//div[@class='userName ng-binding']");
	By celebrationBirth = By.xpath("//table[@class='marginBottom20']/tbody/tr/td[1][@class='ng-binding']");

	Entry<String, String> apiResValues, apiBirthValue;

	// Method to login Clantra and Verify Email
	public void loginClantra(String UUID) throws Exception {

		waitForPageLoad();
		if (checkElementPresence(userEmailID)) {
			waitFindEnterText(userEmailID, UUID);
			assert true;
		} else {
			assert false;
		}
		waitForPageLoad();

		if (checkElementPresence(userPass)) {
			waitFindEnterText(userPass, TestData.PASS);
			assert true;
		} else {
			assert false;
		}
		// sending password
		if (checkElementdisplayed(loginButton)) {
			waitFindClick(loginButton);
			assert true;
		} else {
			LOGGER.info("Not able click on login button");
			assert false;
		}
		waitForPageLoad();
		waitForElement(loginSuccessText);
		CustomLogging.writeToReport(
				"INFO: User logged in with User name: " + TestData.EMAIL_INVALID + "\n password:  successfully");
		CustomAssertions.assertText(textFromApplication(loginSuccessText), TestData.LOGIN_SUCCESSFULLY,
				"Verified Login message as ");

	
		// Creating maps for UI and API responses
		HashMap<String, String> uiBirthData = new HashMap<String, String>();
		HashMap<String,String> getBirthApiValues = GetApiData.getBirthValues();
		
		List<WebElement> birthNames = driver.findElements(celebrationBirth);
		
		for(WebElement ele : birthNames) {
			String[] split = ele.getText().split(" ");
			System.out.println(split[0] + " --- Key UI");
			System.out.println(split[1] + " --- Value UI");
			uiBirthData.put(split[0], split[1]);
		}
		
		// Getting UI Values
		String email_UI = getText(emailText);
		String empID_UI = getText(empId);
		String obj_empID_UI = empID_UI.substring(empID_UI.length() - 7, empID_UI.length() - 1);

		LOGGER.info(getBirthApiValues + " Getting birthday response");
		
		// Verifying API and UI values
		CustomAssertions.assertText(PostApiData.getEmail(), email_UI, "Verified Email Id ");
		CustomAssertions.assertText(PostApiData.getEmployeeId(), obj_empID_UI,"Verified Employee Id ");
		CustomAssertions.assertText(getBirthApiValues, uiBirthData,"Verified Bithday First and Last names");
		
	}
}
