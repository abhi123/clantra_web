package clantraWeb;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;

public class ClantraAttendance extends CommonUtils {

	static final Logger LOGGER = Logger.getLogger(ClantraAttendance.class.getName());

	// Objects
	By attendance = By.xpath("//div[@class='col customSubmitBtn marginLeft23 clearfix']/button");
	By markAttendance = By.xpath("//md-dialog/section/div/div[4][@class='alignCenter']/button");
	By attendanceSuccessGreenText = By.xpath("//span[@class='md-toast-text ng-binding']");

	// Method to marking attendance
	public void markingAttendance() throws Exception {

		waitForPageLoad();
		if (checkElementPresence(attendance)) {
			moveToWebElementAndActionClick(attendance);
			javaScriptScrollUsingBy(markAttendance);
			javaScriptClickUsingBy(markAttendance);
			assert true;
		} else {
			assert false;
		}
		waitForPageLoad();
	}
}
