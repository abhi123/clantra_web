package clantraWeb;

import java.io.File;
import org.apache.log4j.Logger;

public class TestData extends TestBase {
	
	// TestData for Clantra
	static final Logger LOGGER = Logger.getLogger(TestData.class.getName());
	public static final String EMAIL = "abhishek.sharma@prontoitlabs.com";
	public static final String PASS = "Oct@2018";
	static final String LOGIN_SUCCESSFULLY = "Login Successfully";
	static final String EMAIL_INVALID = "abc.sharma@prontoitlabs.com";
	public static final String ATTENDANCE_RED_TEXT = "Attendance for this date has already been marked";  
	static final String LOGIN_ERROR = "Email or password Incorrect!";

	// API
	public static final String GET_BIRTH_API = "http://www.clantra.com/api/v1/dashboard/get-birthdays?organisationId=5888b532e4b0574643565038&pageNumber=0&pageSize=4";
	public static final String TOKEN = "eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC93d3cucHJvbnRvaXRsYWJzLmNvbSIsInN1YiI6Ilx1MDAxM3lLXHUwMDFBRkFPT1x1MDAwNFBoLFx1MDAxOVxiT1x1MDAxRXBAXHUwMDFCQVx1MDAxNE1cdTAwMUVTIiwicm9sZSI6IkVNUExPWUVFIiwiZXhwIjoxNTQyNzQyMjYzLCJpYXQiOjE1Mzg5NDA2NjN9.mcbCVtUPuRmwWldwqWYbDYgN-XKoH2Qh5mW-l9Xj4mU";
	public static final String LOGIN_API = "http://www.clantra.com/api/v1/auth/login";
	public static final String SLACK_URL = "https://slack.com/api/files.upload";
	public static final String SLACK_TOKEN = "Bearer xoxs-7030444480-8241879365-469652745957-cfa057120e8f2ce1aa18a93fa1fabf1efef06a9672138b38aa3f511c5fe5569b";
			
	// URLs
	static final String LOGIN_URL = "http://www.clantra.com/home/";
	
	//File Path
	public static final String USER_HOME = System.getProperty("user.home");
	public static final String OUTPUT_FOLDER = USER_HOME + File.separator + "Downloads" + File.separator;
	public static final String USER_DIR = System.getProperty("user.dir");
	public static final String FILE_PATH = USER_DIR + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "data" + File.separator + "users" + File.separator;
	
	//File Names
	public static final String USERS_DATA =  FILE_PATH + "users.json";
	
}
