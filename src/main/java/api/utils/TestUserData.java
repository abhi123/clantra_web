package api.utils;

import java.io.File;
import java.io.IOException;
import com.fasterxml.jackson.databind.ObjectMapper;
import clantraWeb.TestData;
import data.users.Users;

public class TestUserData {
	static ObjectMapper mapper = new ObjectMapper();
	static Users users = new Users(TestData.EMAIL, TestData.PASS);
	static String usersJsonString;

	public static String getUsersData() {

		try {
			mapper.writeValue(new File(TestData.USERS_DATA), users);
			usersJsonString = mapper.writeValueAsString(users);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return usersJsonString;
	}
}
