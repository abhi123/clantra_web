package api.test;

import java.io.IOException;
import java.util.HashMap;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import api.utils.RestClient;
import api.utils.TestUserData;
import clantraWeb.TestData;
import utils.CustomAssertions;
import utils.CustomLogging;

public class PostApiResponse {

	static final Logger LOGGER = Logger.getLogger(GetApiResponse.class.getName());
	static CloseableHttpResponse closebaleHttpResponse;
	static RestClient restClient;

	public JSONObject postAPITest() throws JsonGenerationException, JsonMappingException, IOException {

		HashMap<String, String> header = new HashMap<String, String>();
		header.put("Content-Type", "application/json");
		header.put("msisdn", "");
		// jackson API: java object to json in String:
		
		restClient = new RestClient();
		closebaleHttpResponse = restClient.post(TestData.LOGIN_API, TestUserData.getUsersData(), header);

		// a. Status Code:
		int statusCode = closebaleHttpResponse.getStatusLine().getStatusCode();

		System.out.println("Status Code--->" + statusCode);
		CustomLogging.writeToReport("INFO: Status Code : " + statusCode + "\n:  successfully");
		CustomAssertions.assertResponseCodeAs200(statusCode, "Verified the LOGIN API response code of ");

		// validate response from API:
		String responseString = EntityUtils.toString(closebaleHttpResponse.getEntity(), "UTF-8");

		JSONObject responseJson = null;
		try {
			responseJson = new JSONObject(responseString);
			System.out.println("The response from API is:" + responseJson);
		} catch (JSONException e) {
			e.printStackTrace();

		}

		return responseJson;
	}

}
