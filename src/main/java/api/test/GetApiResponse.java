package api.test;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import api.response.user.data.PostApiData;
import api.utils.RestClient;
import clantraWeb.TestData;
import net.minidev.json.parser.ParseException;
import utils.CustomAssertions;
import utils.CustomLogging;

public class GetApiResponse {
	
	static final Logger LOGGER = Logger.getLogger(GetApiResponse.class.getName());
	static CloseableHttpResponse closebaleHttpResponse;
	static RestClient restClient;

	PostApiResponse objPostCall = new PostApiResponse();

	public JSONObject getAPITestWithHeaders(String URL)
			throws ClientProtocolException, IOException, ParseException, java.text.ParseException {

		URL url = new URL(URL);
		URLConnection conn = url.openConnection();
		Map<String, List<String>> map = conn.getHeaderFields();
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();

		System.out.println(connection.getRequestProperty("User-Agent"));

		// Map<String, List<String>> map = conn.getHeaderFields();
		URL maps = conn.getURL();
		System.out.println(maps + " Get-Birth-URL ");
		System.out.println("Printing All Response Header for URL: " + url.toString() + "\n");
		for (Map.Entry<String, List<String>> entry : map.entrySet()) {
			System.out.println(entry.getKey() + " : " + entry.getValue());
		}

		HashMap<String, String> headerMap = new HashMap<String, String>();
		System.out.println(PostApiData.getToken());
		headerMap.put("AUTH-TOKEN", PostApiData.getToken());
		headerMap.put("platform", "web");
		// headerMap.put("Authorization",
		// "5ab066f1a0e6210e3668582c5532d128250547b4a9f026abe25264be");
		restClient = new RestClient();
		closebaleHttpResponse = restClient.get(TestData.GET_BIRTH_API, headerMap);

		// a. Status Code:
		int statusCode = closebaleHttpResponse.getStatusLine().getStatusCode();
		System.out.println("Status Code--->" + statusCode);
		CustomLogging.writeToReport("INFO: Status Code : " + statusCode + "\n:  successfully");
		CustomAssertions.assertResponseCodeAs200(statusCode, "Verified the BIRTH API response code of ");
		// b. Json String:
		String responseString = EntityUtils.toString(closebaleHttpResponse.getEntity(), "UTF-8");

		JSONObject responseJson = null;
		try {
			responseJson = new JSONObject(responseString);
			System.out.println("The response from API is:" + responseJson);
		} catch (JSONException e) {
			e.printStackTrace();

		}
		return responseJson;
	}
}
