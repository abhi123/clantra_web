package api.response.user.data;

import java.io.IOException;
import org.json.JSONObject;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

import api.test.PostApiResponse;
import api.utils.TestUtil;

public class PostApiData {

	static PostApiResponse obj = new PostApiResponse();
	static JSONObject responseJson;
	static String getEmail = "/data/userDetailDto/email";
	static String getEmployeeId = "/data/userDetailDto/employeeId";
	static String getToken = "/data/token";
	
	public static String getEmail() throws JsonGenerationException, JsonMappingException, IOException {
		responseJson = obj.postAPITest();
		return TestUtil.getValueByJPath(responseJson, getEmail);
		
	}

	public static String getEmployeeId() throws JsonGenerationException, JsonMappingException, IOException {
		responseJson = obj.postAPITest();
		return TestUtil.getValueByJPath(responseJson, getEmployeeId);

	}

	public static String getToken() throws JsonGenerationException, JsonMappingException, IOException {
		responseJson = obj.postAPITest();
		return TestUtil.getValueByJPath(responseJson, getToken);

	}

}
