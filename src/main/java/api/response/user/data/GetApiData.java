package api.response.user.data;

import java.io.IOException;
import java.util.HashMap;
import org.json.JSONObject;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import api.test.GetApiResponse;
import api.utils.TestUtil;
import clantraWeb.TestData;
import net.minidev.json.parser.ParseException;

public class GetApiData {
	static GetApiResponse obj = new GetApiResponse();
	static HashMap<String, String> empMap = new HashMap<String, String>();
	static JSONObject responseJson;
	static String getDataSize = "/data";

	public static HashMap<String, String> getBirthValues() throws JsonGenerationException, JsonMappingException, IOException, ParseException, java.text.ParseException {
	
		responseJson = obj.getAPITestWithHeaders(TestData.GET_BIRTH_API);
		int size = TestUtil.getValueByJPath(responseJson, getDataSize).length();
		System.out.println(size);
		try {
			for (int i = 0; i <= size; i++) {

				// Getting first Value response
				String firstName = TestUtil.getValueByJPath(responseJson, "/data[" + i + "]/employeeFirstName");
				String lastName = TestUtil.getValueByJPath(responseJson, "/data[" + i + "]/employeeLastName");

				System.out.println(firstName + " " + lastName);
				empMap.put(firstName, lastName);
			}
		} catch (Exception e) {
			
			System.out.println(e.getMessage() + " Employee names are not found !!");
		}
		return empMap;
	}
}
